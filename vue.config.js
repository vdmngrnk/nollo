const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: process.env.NODE_ENV === 'production'
    ? '/nollo/'
    : '/',
  transpileDependencies: [],
	productionSourceMap: process.env.NODE_ENV == 'production' ? false : true,
})
