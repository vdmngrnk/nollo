"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vueRouter = require("vue-router");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var routes = [{
  path: '/',
  name: 'Home',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/Index'));
    });
  },
  meta: {
    title: 'home',
    key: 1
  }
}, {
  path: '/place',
  name: 'Place',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/Place'));
    });
  },
  meta: {
    title: 'place',
    key: 2
  }
}, {
  path: '/booking',
  name: 'Booking',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/Booking'));
    });
  },
  meta: {
    title: 'booking',
    key: 3
  }
}, {
  path: '/popular',
  name: 'Popular',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/popular/Index'));
    });
  },
  meta: {
    title: 'popular',
    key: 3
  }
}, {
  path: '/popular-card',
  name: 'PopularCard',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/popular/PopularCard'));
    });
  },
  meta: {
    title: 'PopularCard',
    key: 4
  }
}, {
  path: '/profile',
  name: 'Profile',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/profile/Index'));
    });
  },
  props: true,
  meta: {
    title: 'Profile',
    key: 5
  } // children: [{
  //     path: 'reservation',
  //     name: 'Reservation',
  //     component: () => import('@/views/profile/Reservation'),
  //   },
  //   {
  //     path: '/added',
  //     name: 'Added',
  //     component: () => import('@/views/profile/Added'),
  //   },
  // ]

}, {
  path: '/create',
  name: 'Create',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/profile/Create'));
    });
  }
}, // {
// path: "/:catchAll(.*)",
// component: NotFound,
// },
{
  path: '/reservation',
  name: 'Reservation',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/Reservation'));
    });
  },
  meta: {
    title: 'Reservation',
    key: 6
  }
}, {
  path: '/create',
  name: 'Create',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/profile/Create'));
    });
  },
  meta: {
    title: 'Create',
    key: 7
  }
}, {
  path: '/added',
  name: 'Added',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/profile/Added'));
    });
  },
  meta: {
    title: 'Added',
    key: 8
  }
}, {
  path: '/terms',
  name: 'Terms',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/Terms'));
    });
  },
  meta: {
    title: 'Terms',
    key: 9
  }
}, {
  path: '/about',
  name: 'About',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/About'));
    });
  },
  meta: {
    title: 'About',
    key: 9
  }
}, {
  path: '/contacts',
  name: 'Contacts',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/Contacts'));
    });
  },
  meta: {
    title: 'Contacts',
    key: 9
  }
}, {
  path: '/authorization',
  name: 'Authorization',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/Authorization'));
    });
  },
  meta: {
    title: 'Authorization',
    key: 10
  }
}, {
  path: '/found',
  name: 'Found',
  component: function component() {
    return Promise.resolve().then(function () {
      return _interopRequireWildcard(require('@/views/Found'));
    });
  },
  props: true,
  meta: {
    title: 'Found',
    key: 11
  }
} // {
//   path: '/recovery',
//   name: 'Recovery',
//   component: () => import('@/views/Recovery'),
//   meta: {
//     title: 'Recovery',
//     key: 12
//   }
// },
];
var router = (0, _vueRouter.createRouter)({
  history: (0, _vueRouter.createWebHistory)(process.env.BASE_URL),
  // base: process.env.BASE_URL,
  routes: routes,
  scrollBehavior: function scrollBehavior() {
    return {
      top: 0
    };
  }
}); // router.afterEach((to) => {
//   Vue.nextTick(() => {
//     document.title = to.meta.title;
//   })
// });

var _default = router;
exports["default"] = _default;