import './assets/styles/imports.styl'
import {
  createApp
} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import vSelect from "../node_modules/vue-select-3/src";
import "../node_modules/vue-select-3/src/scss/vue-select.scss";
import vMessage from "./components/vue-custom/VMessage.vue";
import vInput from "./components/vue-custom/vInput.vue";
import CustomSelect from "./components/vue-custom/CustomSelect.vue";
// import {VTooltip} from 'v-tooltip'
import tooltip from "./helpers/tooltip.js";
import PerfectScrollbar from "vue3-perfect-scrollbar";
import "vue3-perfect-scrollbar/dist/vue3-perfect-scrollbar.css";

createApp(App)
  .directive('tooltip', tooltip)
  .component("v-select", vSelect)
  .component("v-message", vMessage)
  .component("v-input", vInput)
  .component("custom-select", CustomSelect)
  .use(store)
  .use(router)
  .use(PerfectScrollbar)
  .mount('#app')