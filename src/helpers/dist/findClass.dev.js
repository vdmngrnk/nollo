"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = findClass;

var _vueRouter = require("vue-router");

var _vuex = require("vuex");

var _runtimeCore = require("@vue/runtime-core");

var routeListLight = ['Create', 'Added', 'Booking'];
var routeListGradient = ['Home', 'Found', 'Popular'];
var routeSkipMobileList = ['Create'];

function findClass() {
  var router = (0, _vueRouter.useRoute)();
  var store = (0, _vuex.useStore)();
  var mobile = (0, _runtimeCore.computed)(function () {
    return store.getters["common/getMobile"];
  });
  var checkRoute = (0, _runtimeCore.computed)(function () {
    if (Object.values(routeSkipMobileList).includes(router.name) && mobile.value) {
      return;
    } else {
      if (Object.values(routeListLight).includes(router.name)) {
        return 'light';
      }
    }

    if (Object.values(routeListGradient).includes(router.name)) {
      return 'gradient';
    }

    if (router.name === 'Authorization') {
      return 'log-in';
    }

    if (router.name === 'Contacts') {
      return 'content-white';
    }

    return '';
  });
  return {
    checkRoute: checkRoute
  };
}