import { useRoute } from "vue-router";
import { useStore } from "vuex";
import { computed } from "@vue/runtime-core";

const routeListLight = [
  'Create',
  'Added',
  'Booking'
]

const routeListGradient = [
  'Home',
  'Found',
  'Popular'
]

const routeSkipMobileList = [
  'Create',
]

export default function findClass () {
  const router = useRoute();
  const store = useStore();
  const mobile = computed(() => store.getters["common/getMobile"]);

  const checkRoute = computed(() =>{
    if (Object.values(routeSkipMobileList).includes(router.name) && mobile.value) {
      return 
    } else {
      if (Object.values(routeListLight).includes(router.name)) {
        return 'light'
      }
    }

    if (Object.values(routeListGradient).includes(router.name)) {
      return 'gradient'
    }

    if (router.name === 'Authorization') {
      return 'log-in'
    }

    if (router.name === 'Contacts') {
      return 'content-white'
    }
    

    return ''
  })

  return {checkRoute}
}