import {ProfileIcon, CalendarIcon, HeartIcon, HomeIcon, MailIcon} from '@/layout/icon/index'
import PersonalData from "@/components/profile/PersonalData"
import Favorites from "@/components/profile/Favorites"
import Places from "@/components/profile/Places"
import ProfileBooking from "@/components/profile/ProfileBooking";
import ProfileMessages from "@/components/profile/ProfileMessages";

export default [
  {
    id: 0,
    name: PersonalData,
    title: 'Данные профиля',
    icon: ProfileIcon
  },
  {
    id: 1,
    name: Favorites,
    title: 'Избранное',
    icon: HeartIcon
  },
  {
    id: 2,
    name: Places,
    title: 'Мои турбазы',
    icon:  HomeIcon
  },
  {
    id: 3,
    name: ProfileBooking,
    title: 'Бронирования',
    icon:  CalendarIcon
  },
  {
    id: 4,
    name: ProfileMessages,
    title: 'Сообщения',
    icon:  MailIcon
  },
]