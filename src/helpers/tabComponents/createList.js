import BasicData from "@/components/create/Index"
import Categories from "@/components/create/Categories"
import Photos from "@/components/create/Photos"
import Contacts from "@/components/create/Contacts"
import Addrress from "@/components/create/Address"
import Rooms from "@/components/create/Rooms"
import Waters from "@/components/create/Waters"
import Info from "@/components/create/Info"

// export default [
export default [{
    id: 0,
    name: BasicData,
    title: 'Основные данные',
    filled: false
  },
  {
    id: 1,
    name: Categories,
    title: 'Категории',
    filled: false
  },
  {
    id: 2,
    name: Photos,
    title: 'Фотографии',
    filled: false
  },
  {
    id: 3,
    name: Contacts,
    title: 'Контактные данные',
    filled: false
  },
  {
    id: 4,
    name: Addrress,
    title: 'Адрес',
    filled: false
  },
  {
    id: 5,
    name: Rooms,
    title: 'Номера',
    filled: false
  },
  {
    id: 6,
    name: Waters,
    title: 'Близлежащие водоемы',
    filled: false
  },
  {
    id: 7,
    name: Info,
    title: 'Информация для гостей',
    filled: false
  },
]

