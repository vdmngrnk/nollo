"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _Index = _interopRequireDefault(require("@/components/create/Index"));

var _Categories = _interopRequireDefault(require("@/components/create/Categories"));

var _Photos = _interopRequireDefault(require("@/components/create/Photos"));

var _Contacts = _interopRequireDefault(require("@/components/create/Contacts"));

var _Address = _interopRequireDefault(require("@/components/create/Address"));

var _Rooms = _interopRequireDefault(require("@/components/create/Rooms"));

var _Waters = _interopRequireDefault(require("@/components/create/Waters"));

var _Info = _interopRequireDefault(require("@/components/create/Info"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// export default [
var _default = [{
  id: 0,
  name: _Index["default"],
  title: 'Основные данные',
  filled: false
}, {
  id: 1,
  name: _Categories["default"],
  title: 'Категории',
  filled: false
}, {
  id: 2,
  name: _Photos["default"],
  title: 'Фотографии',
  filled: false
}, {
  id: 3,
  name: _Contacts["default"],
  title: 'Контактные данные',
  filled: false
}, {
  id: 4,
  name: _Address["default"],
  title: 'Адрес',
  filled: false
}, {
  id: 5,
  name: _Rooms["default"],
  title: 'Номера',
  filled: false
}, {
  id: 6,
  name: _Waters["default"],
  title: 'Близлежащие водоемы',
  filled: false
}, {
  id: 7,
  name: _Info["default"],
  title: 'Информация для гостей',
  filled: false
}];
exports["default"] = _default;