"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = tabComponents;

var _vue = require("vue");

var _createList = _interopRequireDefault(require("@/helpers/tabComponents/createList"));

var _profileList = _interopRequireDefault(require("@/helpers/tabComponents/profileList"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var componentList = {
  create: _createList["default"],
  profile: _profileList["default"]
};

function tabComponents(page, startIndex) {
  var currentList = componentList[page];
  var currentComponent = (0, _vue.ref)(currentList[startIndex]);

  var findComponent = function findComponent(id) {
    currentComponent.value.filled = true;
    currentComponent.value = currentList.find(function (item) {
      return item.id === id;
    });
  };

  return {
    componentList: currentList,
    currentComponent: currentComponent,
    findComponent: findComponent
  };
}