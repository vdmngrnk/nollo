import {
  ref,
} from 'vue'
import createList from "@/helpers/tabComponents/createList";
import profileList from "@/helpers/tabComponents/profileList";

const componentList = {
  create: createList,
  profile: profileList
}

export default function tabComponents(page, startIndex) {
  const currentList = componentList[page]
  const currentComponent = ref(currentList[startIndex]);

  const findComponent = (id) => {
    currentComponent.value.filled = true
    currentComponent.value = currentList.find(item => item.id === id);
  }

  return {
    componentList: currentList,
    currentComponent,
    findComponent
  }
}