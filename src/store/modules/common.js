const module = {
  namespaced: true,

  state: () => ({
    mobile:  null,
    desktop: null,
  }),

  getters: {
    getMobile(state) {
      return state.mobile
    },
    getDesktop(state) {
      return state.desktop
    },
  },

  actions: {
		handleResize({state}) {
			if(window.innerWidth <= 1025) {
				state.mobile = true
			} else {
				state.mobile =  false
			}

      if(window.innerWidth >= 1440) {
        state.desktop = true
      } else {
        state.desktop = false
      }
		},
	}
}

export default module