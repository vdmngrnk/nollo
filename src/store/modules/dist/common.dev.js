"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _module = {
  namespaced: true,
  state: function state() {
    return {
      mobile: null,
      desktop: null
    };
  },
  getters: {
    getMobile: function getMobile(state) {
      return state.mobile;
    },
    getDesktop: function getDesktop(state) {
      return state.desktop;
    }
  },
  actions: {
    handleResize: function handleResize(_ref) {
      var state = _ref.state;

      if (window.innerWidth <= 1025) {
        state.mobile = true;
      } else {
        state.mobile = false;
      }

      if (window.innerWidth >= 1440) {
        state.desktop = true;
      } else {
        state.desktop = false;
      }
    }
  }
};
var _default = _module;
exports["default"] = _default;