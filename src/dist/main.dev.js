"use strict";

require("./assets/styles/imports.styl");

var _vue = require("vue");

var _App = _interopRequireDefault(require("./App.vue"));

var _router = _interopRequireDefault(require("./router"));

var _store = _interopRequireDefault(require("./store"));

var _src = _interopRequireDefault(require("../node_modules/vue-select-3/src"));

require("../node_modules/vue-select-3/src/scss/vue-select.scss");

var _VMessage = _interopRequireDefault(require("./components/vue-custom/VMessage.vue"));

var _vInput = _interopRequireDefault(require("./components/vue-custom/vInput.vue"));

var _CustomSelect = _interopRequireDefault(require("./components/vue-custom/CustomSelect.vue"));

var _tooltip = _interopRequireDefault(require("./helpers/tooltip.js"));

var _vue3PerfectScrollbar = _interopRequireDefault(require("vue3-perfect-scrollbar"));

require("vue3-perfect-scrollbar/dist/vue3-perfect-scrollbar.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import {VTooltip} from 'v-tooltip'
(0, _vue.createApp)(_App["default"]).directive('tooltip', _tooltip["default"]).component("v-select", _src["default"]).component("v-message", _VMessage["default"]).component("v-input", _vInput["default"]).component("custom-select", _CustomSelect["default"]).use(_store["default"]).use(_router["default"]).use(_vue3PerfectScrollbar["default"]).mount('#app');